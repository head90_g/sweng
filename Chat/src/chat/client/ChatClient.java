package chat.client;

import chat.server.ChatHandlerImpl;
import chat.common.ChatMessage;

import java.rmi.RemoteException;

/**
 * Created by giuseppetesta on 31/05/16.
 */
public class ChatClient extends ChatHandlerImpl {
    private static final long serialVersionUID = 7098864620078765408L;

    private final String name;

    public ChatClient(String name) throws RemoteException {
        super();

        this.name = name;
    }

    @Override
    public void notifyMessage(ChatMessage msg) throws RemoteException {
        System.out.println(msg.getName() + ": " + msg.getMessage());
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }
}
