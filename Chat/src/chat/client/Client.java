package chat.client;

import chat.common.ChatService;
import chat.common.MessageSink;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by giuseppetesta on 31/05/16.
 */

public class Client {
    /** Run with - Djava.security.policy=client.policy */
    public static void main(String[] args) throws NotBoundException, IOException {
        //System.setSecurityManager(new RMISecurityManager());

        ChatClient cli = new ChatClient("Luca" + (int)(Math.random() * 100));

        ChatService srv = (ChatService) Naming.lookup("ChatServer");
        MessageSink snk = srv.register(cli);

        BufferedReader rdr = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = rdr.readLine();
            if (line == null)
                break;
            snk.sendAll(line);
        }

        srv.unregister(cli);
        UnicastRemoteObject.unexportObject(cli, false);

        System.out.println("Session ended");
    }
}