package chat.common;

/**
 * Created by giuseppetesta on 31/05/16.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/** Chat message handler.
 * The parties interested in the chat messages should
 * provide this interface to the server.
 */
public interface ChatHandler extends Remote {
    /** Symbolic name of the handler */
    String getName() throws RemoteException;
    /** Callback for delivering chat messages.
     * The server uses the callback to notify the client that a new
     * message is available.
     *
     * @param msg A message directed to this client.
     */
    void notifyMessage(ChatMessage msg) throws RemoteException;
}
