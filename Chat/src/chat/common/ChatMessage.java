package chat.common;

import java.io.Serializable;

/**
 * Created by giuseppetesta on 31/05/16.
 */

public class ChatMessage implements Serializable {
    private static final long serialVersionUID = -1447272226706036473L;

    private final String name;
    private final String message;

    public ChatMessage(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}
