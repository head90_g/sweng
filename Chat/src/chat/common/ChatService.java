package chat.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by giuseppetesta on 31/05/16.
 *
/** Remote chat service.
 * This interface is used by clients to register
 * and unregister themselves from the chat system.
 */

public interface ChatService extends Remote {
    MessageSink register(ChatHandler client) throws RemoteException;
    void unregister(ChatHandler client) throws RemoteException;
}
