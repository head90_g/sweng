package chat.common;

/**
 * Created by giuseppetesta on 31/05/16.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/** Chat message sink.
 * The client uses the sink to send a message to the
 * other participants.
 */
public interface MessageSink extends Remote {
    /** Send a message to all the users.
     *
     * @param msg The message.
     */
    void sendAll(String msg) throws RemoteException;
}
