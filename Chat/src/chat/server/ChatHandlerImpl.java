package chat.server;

import chat.common.ChatHandler;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by giuseppetesta on 31/05/16.
 */

public abstract class ChatHandlerImpl extends UnicastRemoteObject implements ChatHandler {
    private static final long serialVersionUID = -6357008026548876360L;

    public ChatHandlerImpl() throws RemoteException {
        super();
    }
}

