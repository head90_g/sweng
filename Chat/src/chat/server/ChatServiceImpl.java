package chat.server;

import chat.common.ChatHandler;
import chat.common.ChatMessage;
import chat.common.ChatService;
import chat.common.MessageSink;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Created by giuseppetesta on 31/05/16.
 */
public class ChatServiceImpl extends UnicastRemoteObject implements ChatService {
    private static final long serialVersionUID = 692162259044298400L;

    private ArrayList<ChatHandler> clients;

    public ChatServiceImpl() throws RemoteException {
        super();

        clients = new ArrayList<ChatHandler>();
    }

    /** Register a client
     *
     * @param client A chat client
     */
    @Override
    public MessageSink register(ChatHandler client) throws RemoteException {
        synchronized (clients) {
            clients.add(client);
        }

        broadcast(new ChatMessage("SERVER", "New client: " + client.getName()));

        return new MessageSinkImpl(client, this);
    }

    /** Unregister a client.
     *
     * @param client A chat client.
     */
    @Override
    public void unregister(ChatHandler client) throws RemoteException {
        synchronized (clients) {
            clients.remove(client);
        }
    }

    public void broadcast(ChatMessage msg) {
        ArrayList<ChatHandler> dead = new ArrayList<ChatHandler>();

        synchronized (clients) {
            for (ChatHandler cli: clients) {
                try {
                    if (cli.getName().equals(msg.getName()))
                        continue;
                    cli.notifyMessage(msg);
                } catch (RemoteException e) {
                    dead.add(cli);
                }
            }

            for (ChatHandler cli: dead)
                clients.remove(cli);
        }
    }
}