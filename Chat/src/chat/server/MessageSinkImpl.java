package chat.server;

import chat.common.ChatHandler;
import chat.common.ChatMessage;
import chat.common.MessageSink;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by giuseppetesta on 31/05/16.
 */

public class MessageSinkImpl extends UnicastRemoteObject implements MessageSink {
    private static final long serialVersionUID = -7885267012523457505L;

    private final ChatServiceImpl notifier;
    private final String name;

    MessageSinkImpl(ChatHandler cli, ChatServiceImpl notifier) throws RemoteException {
        super();

        this.notifier = notifier;
        this.name = cli.getName();
    }

    public void sendAll(String msg) throws RemoteException {
        notifier.broadcast(new ChatMessage(name, msg));
    }
}
