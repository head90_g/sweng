package chat.server;

import chat.common.ChatMessage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by giuseppetesta on 31/05/16.
 */
public class Server {
    private static final long serialVersionUID = 8573201216151204858L;

    /** Run with:
     -Djava.security.policy=server.policy
     -Djava.rmi.server.codebase="file://${workspace_loc:/Server}/bin/ file://${workspace_loc:/Common}/bin/"
     */

    public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException {
        //System.setSecurityManager(new RMISecurityManager());

        final ChatServiceImpl srv = new ChatServiceImpl();

        Naming.rebind("ChatServer", srv);

        System.out.println("Press enter to terminate");
        try {
            System.in.read();
        } catch (IOException e) {
			/* Don't care */
        }

        Naming.unbind("ChatServer");

        srv.broadcast(new ChatMessage("SERVER", "Going down..."));

        UnicastRemoteObject.unexportObject(srv, false);
    }
}
