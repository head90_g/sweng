import java.util.Random;
import java.util.Date;
import java.math.*;

public class GestisciSerra{
	
	
	/* controllo sul tempo:
	 * per semplicità ho impostato che la temperatura non si possa modificare meno di 1 minuto dall'ultima modifica invece che 30 min.
	 * La prima volta che si accede alla serra bisogna aspettare 1 minuto
	 */
	
	private Date lastUpdateTemp = new Date();
	private Date lastUpdateUm = new Date();

	public static long min = 60000; // 1 min in ms
		

	public void modificaTemperatura(int a) { // inserisci 1 pe ralzare 0 per abbassare
		
	    Date oraCorrente = new Date();
	    long diff = (oraCorrente.getTime() - lastUpdateUm.getTime());
		
		if( diff >= min ){
			
			if( a == 1 ){		
				Serra.getInstance().setTemperatura(Serra.getInstance().getTemperatura() + 0.5);
				System.out.println("\nLa temperatura è stata aumentata\n"
					+ "Temperatura attuale: " + Serra.getInstance().getTemperatura());
			}	else {
				Serra.getInstance().setTemperatura(Serra.getInstance().getTemperatura() - 0.5);
				System.out.println("\nLa temperatura è stata diminuita\n"
					+ "Temperatura attuale: " + Serra.getInstance().getTemperatura());
			
			}
			
			lastUpdateTemp = oraCorrente;
		
		}else{
			
			long manca = ((min - diff) / 1000) ;
			System.out.println("Non puoi modificare la temperatura ora perchè\n"
					+ "hai modificato la temperatura meno di 1 minuto fa\n"
					+ "devi aspettare ancora: " + manca + " secondi prima "
							+ "di poter mdificare la temperatura di nuovo!");
			
		}
		
	}
	
	public void modificaUmidità(int a) { // inserisci 1 pe ralzare 0 per abbassare
		
		Date oraCorrente = new Date();
	    long diff = (oraCorrente.getTime() - lastUpdateUm.getTime());
	    
	    if( diff >= min ){
	    
	    	Random random = new Random();
	    	int r = random.nextInt(5) + 1;

	    	if( a == 1 ){
	    		if( r == 1 ){
	    			Serra.getInstance().setTemperatura(Serra.getInstance().getTemperatura() + 0.5);
	    			Serra.getInstance().setUmidità(Serra.getInstance().getUmidità() + 5);
	    			System.out.println("\nL'umidità è stata aumentata\n"
	    					+ "Attenzione: anche la temperatura è cambiata\n"
			    		+ "Umidità attuale: " + Serra.getInstance().getUmidità() +  "\nTemperatura attuale: " + Serra.getInstance().getTemperatura());
	    		}else {  
	    			Serra.getInstance().setUmidità(Serra.getInstance().getUmidità() + 5);
	    			System.out.println("\nl'umidità è stata aumentata\n"
						+ "Umidità attuale: " + Serra.getInstance().getUmidità());
	    		}
	    	}	else {
	    		if( r == 1 ){
	    			Serra.getInstance().setTemperatura(Serra.getInstance().getTemperatura() - 0.5);
	    			Serra.getInstance().setUmidità(Serra.getInstance().getUmidità() - 5);
	    			System.out.println("\nl'umidità è stata diminuita\n"
	    					+ "Attenzione: anche la temperatura è cambiata\n"
						+ "Umidità attuale: " + Serra.getInstance().getUmidità() +  "\nTemperatura attuale: " + Serra.getInstance().getTemperatura());
	    		}else {  
	    			Serra.getInstance().setUmidità(Serra.getInstance().getUmidità() + 5);
	    			System.out.println("\nl'umidità è stata diminuita\n"
	    					+  "Umidità attuale: " + Serra.getInstance().getUmidità());
		}
		}
	    
	    	lastUpdateUm = oraCorrente;
	    	
	    }else{
	    	
			long manca = ((min - diff) / 1000) ;
			System.out.println("Non puoi modificare l'umidità ora perchè\n"
					+ "hai modificato la temperatura meno di 1 minuto fa\n"
					+ "devi aspettare ancora: " + manca + " secondi prima "
							+ "di poter mdificare l'umidità di nuovo!");

	    }
		
	}
	
		public void visualizzaDati(){
			
			System.out.println("\nDATI  SERRA:\nSpazio libero: " + Serra.getInstance().getSpazioLibero() +
					"\nTemperatura: " + Serra.getInstance().getTemperatura() + 
					 "\nUmidità:"  + Serra.getInstance().getUmidità() + " %" +
					"\nGiorno: " + Serra.getInstance().giorno); 
			
			int k = 1; 
			System.out.println("\n\n                                                  SERRA");
			while( k <= 243) { System.out.print( '-' ); k++;}
			System.out.println();
			
			for( int i=0 ; i < Serra.getInstance().pianteSerra.length ; i++) {
				
				for( int j=0 ; j < Serra.getInstance().pianteSerra[i].length ; j++) {
					
					if ( Serra.getInstance().pianteSerra[i][j] == null){
						
						System.out.print("| [" + Integer.toString(i+1)+ "][" + Integer.toString(j+1) + "]:sp.libero" + " |" );
						
					}else { 
						
						System.out.print("| [" + Integer.toString(i+1)+ "][" + Integer.toString(j+1) + "]:" + Serra.getInstance().pianteSerra[i][j].getID() + "  |" ); } 

				}
				
				System.out.println();
			}
			k = 1;
			while( k <= 243) { System.out.print( '-' ); k++;}
			System.out.println();
				
		}
		
		// per semplicità ho ipotizzato che le piante si possano inseriere in modo da accupare solo celle consecutive.

		public void inserisciPianta(Pianta p, int[] posizione){

		    for(int i = 0; i < p.getOccupazione(); i++) {	Serra.getInstance().pianteSerra[posizione[0]-1][posizione[1]-i-1] = p; }
	    				    		
	}
		
		// bisogna inserire la prima posizione della pianta 
		
		public void rimuoviPianta(int a , int b){
			
			int o = Serra.getInstance().pianteSerra[a][b].getOccupazione();
			Serra.getInstance().pianteSerra[a][b] = null;
			
			for(int i = 1; i < (o+1); i++) {	
			
				Serra.getInstance().pianteSerra[a][b+i] = null; }
		}
		

		// funzione che controlla se ci sono sp caselle consecutive libere.
		
		public int[] checkSpazio( int sp ){
			
			int a = 0;   boolean spazio = false;
			int i = 0; int j = 0; int count=0;
			int [] posizione = new int[2];
						
			while( i < Serra.getInstance().pianteSerra.length && spazio == false) {
				while(  j < Serra.getInstance().pianteSerra[i].length && spazio == false) {

					++count;
					if(Serra.getInstance().pianteSerra[i][j] == null ){

						a++;

						if( a == sp) { 
							
							spazio = true; 
							posizione[0]=i+1;
							posizione[1]=j+1;
						}
						
					}else{ a=0; }
				j++;	
				}
				
				j=0;	 i++;	a=0; //cerca su ogni riga =)
			}
			
			if (spazio) { return posizione;  }
			
			else {
				posizione[0]=0;
				posizione[1]=0;
				return posizione;}
		}
		
		
		public void visualizzaDatiPianta(int x, int y){
			
			if(Serra.getInstance().pianteSerra[x][y] == null)
				System.out.println("La posizione scelta è vuota!");
			else{
				System.out.println("\nDati pianta :\n\nNome:   " +  Serra.getInstance().pianteSerra[x][y].getName() +"\nEtà [anni] :  " + 
						Serra.getInstance().pianteSerra[x][y].getEtà() + "\nAltezza: " + Serra.getInstance().pianteSerra[x][y].getAltezza() + 
						"\nCategoria: " + Serra.getInstance().pianteSerra[x][y].getCat() 
						+ "\nSpazio occupato [numero celle] : "  
						+ Serra.getInstance().pianteSerra[x][y].getOccupazione() + "\nStato salute: " 
						+ Serra.getInstance().pianteSerra[x][y].stati[Serra.getInstance().pianteSerra[x][y].getStatoPianta()]
						+ "\nGiorni negativi: " + Serra.getInstance().pianteSerra[x][y].getNeg()
						+ "\nGiorni positivi: " + Serra.getInstance().pianteSerra[x][y].getPos());				
			}
			
		}
		
		public void avanzamentoTemporale(){
			
			double tempAttuale = Serra.getInstance().getTemperatura();
			
			int i = 0; int j = 0;
			
			while( i < Serra.getInstance().pianteSerra.length ) {
				System.out.println(Serra.getInstance().pianteSerra.length);
				while(  j < Serra.getInstance().pianteSerra[i].length ) {
					System.out.println(Serra.getInstance().pianteSerra[i].length);

					if(Serra.getInstance().pianteSerra[i][j] != null ){
						
						int temp = Serra.getInstance().pianteSerra[i][j].getOccupazione();
						
						if(tempAttuale < Serra.getInstance().pianteSerra[i][j].getUpper() && tempAttuale > Serra.getInstance().pianteSerra[i][j].getLower()){
							
							Serra.getInstance().pianteSerra[i][j].setPos(Serra.getInstance().pianteSerra[i][j].getPos()+1);
						
						} else { Serra.getInstance().pianteSerra[i][j].setNeg(Serra.getInstance().pianteSerra[i][j].getNeg()+1); }
						
						if(Serra.getInstance().pianteSerra[i][j].getPos() == 10 && Serra.getInstance().pianteSerra[i][j].getStatoPianta() < 4){
							
							Serra.getInstance().pianteSerra[i][j].setStatoPianta(Serra.getInstance().pianteSerra[i][j].getStatoPianta()+1);
							Serra.getInstance().pianteSerra[i][j].setPos(0);
							
						}
						
						if(Serra.getInstance().pianteSerra[i][j].getNeg() == 10 && Serra.getInstance().pianteSerra[i][j].getStatoPianta() > 1){
							
							Serra.getInstance().pianteSerra[i][j].setStatoPianta(Serra.getInstance().pianteSerra[i][j].getStatoPianta()-1);
							Serra.getInstance().pianteSerra[i][j].setNeg(0);
						}
						
						if(Serra.getInstance().pianteSerra[i][j].getNeg() == 10 && Serra.getInstance().pianteSerra[i][j].getStatoPianta() == 1){
							
							rimuoviPianta(i,j);
							
						}
						
						j = j + temp;	
						

					}else { j++; }
				
					System.out.println("\nriga " + i + "\ncolonna " + j);
				}
				
				i++; j=0;
			}
			
			Serra.getInstance().giorno++;
					
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
