abstract public class Pianta {
	
	private int upperBound;
	private int lowerBound;
	
	private int ggPos; 
	private int ggNeg;
	
	private String nome;
	
	public static String[] stati = {"0-MORTA","1-SCARSO","2-SUFFICIENTE","3-BUONO","4-OTTIMO"};

	private int stato = 3; /** Lo stato corrisponde ai valori della classe stati */
    
	private String cat; /** Le categorie possibili sono {Tropicale, Temperata, Invernale} */
	
	private int età;
	
	private int occupazione; /** occupazione può assumere solo i valori {1, 2, 4} */
	
	private int altezza; /** L'altezza è espressa in cm */
	
	
	private String ID;

	public Pianta(int occ, String nome, String cat, int eta, int altezza){
		this.età = eta;
		this.cat = cat;
		this.nome = nome;
		this.occupazione = occ;
		this.altezza = altezza;
	}
		
	// Getter e setter per i campi statici
	
	public String getName() { return nome;}
	public void setName(String newNome) { nome = newNome; } 
	
	public int getStatoPianta() { return stato; }
	public void setStatoPianta( int newStato) { stato = newStato; }
	
	public String getCat() { return cat; }
	public void setCat(String newCat) { cat = newCat; } 
	
	public String getID() { return ID; }
	public void setID(String newID) { ID = newID; }
	
	public int getAltezza() { return altezza;}
	public void setAltezza(int newAltezza) { altezza = newAltezza; } 
	
	public int getEtà() { return età; }
	public void setEtà( int newEtà) { età = newEtà; }
		
	public int getOccupazione() { return occupazione; }
	public void setOccupazione( int newOcc) { età = newOcc; }
	
	public int getUpper() { return upperBound;}
	public void setUpper( int upper) { upperBound = upper; }
	
	public int getLower() { return lowerBound; }
	public void setLower( int lower) { lowerBound = lower; }
	
	public int getNeg() { return ggNeg;}
	public void setNeg( int neg) { ggNeg = neg; }
	
	public int getPos() { return ggPos; }
	public void setPos( int pos) { ggPos = pos; }

}

