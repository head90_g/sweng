public class PiantaInvernale extends Pianta{
	
	public PiantaInvernale(int occ, String nome, String cat, int eta, int altezza){
		
		super( occ,  nome,  cat, eta, altezza); //in questo modo chiamo il costruttore della superclasse e aggiungo i campi che mancano
	
		setCat("Invernale");
		
		setLower(-50);
		setUpper(16);
		
		setPos(0);
		setNeg(0);
			        
	}

}