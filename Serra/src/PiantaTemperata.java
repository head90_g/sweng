public class PiantaTemperata extends Pianta{
	
	public PiantaTemperata(int occ, String nome, String cat, int eta, int altezza){
		
		super(occ,nome,cat,eta,altezza);
		
		setCat("Temperata");

		setLower(14);
		setUpper(28);
		
		setPos(0);
		setNeg(0);

	}
	
}