public class PiantaTropicale extends Pianta{
	
	public PiantaTropicale(int occ, String nome, String cat, int eta, int altezza){
		
		super(occ,nome,cat,eta,altezza);
	
		setCat("Tropicale");

		setLower(25);
		setUpper(50);
		
		setPos(0);
		setNeg(0);
		
	}
	
}