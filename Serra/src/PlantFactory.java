import java.util.Scanner;
import java.util.*;

public class PlantFactory {
	
	private int count = 1;

	
	public Pianta makePianta(int occ, String nome, String cat, int eta, int altezza){
				
		Pianta laPianta = null;
		
		if(cat.equals("Tropicale")){
			
			laPianta = new PiantaTropicale(occ,nome,cat,eta,altezza);
						
		} else if (cat.equals("Temperata")){
			
			laPianta = new PiantaTemperata(occ,nome,cat,eta,altezza);

		}else if (cat.equals( "Invernale")){
			
			laPianta = new PiantaInvernale(occ,nome,cat,eta,altezza);

		} 

		laPianta.setName(nome);
		laPianta.setOccupazione(occ);
		laPianta.setEtà(eta);
		laPianta.setAltezza(altezza);
		laPianta.setID("Pianta:"+Integer.toString(count));
		count++;

						
			System.out.println("\nDati pianta inserita:\n"+ 
					"\nNome:   " +  laPianta.getName() + 
					"\nEtà [anni] :  " + laPianta.getEtà() +
					"\nAltezza [cm]: " + laPianta.getAltezza() +
					"\nCategoria: " + laPianta.getCat() + 
					"\nSpazio occupato [numero celle] : "  + laPianta.getOccupazione() + 
					"\nStato salute: " + laPianta.stati[laPianta.getStatoPianta()] );

			
			return laPianta;
	}

}


