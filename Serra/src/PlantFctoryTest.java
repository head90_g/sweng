import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PlantFctoryTest {
    
    public PlantFctoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test del metodo makePianta, della classe PlantFactory.
     */
    @Test
    public void testmakePianta() {
        System.out.println("makePlant");
        String categoria = "Tropicale";
        int dimensione = 2;
        int età = 20;
        int altezza = 150;
        String nome = "Pianta1";
        PlantFactory instance = new PlantFactory();
        Pianta result = instance.makePianta(dimensione, nome, categoria ,età , altezza);
        assertEquals(categoria, result.getCat());
        assertEquals(dimensione, result.getOccupazione() );
        assertEquals(nome, result.getName());
        assertEquals(età, result.getEtà() );
        assertEquals(altezza, result.getAltezza() );
        
    }
    
}
