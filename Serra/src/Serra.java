 import java.util.Arrays;
import java.util.Date;

public class Serra{
	
	public int giorno = 0;  
	
	
	static int lunghezzaSerra = 12;
	static int larghezzaSerra = 3;
	Pianta [][] pianteSerra = new Pianta[larghezzaSerra][lunghezzaSerra];  // arraylist??

	private double temperatura;
	private double umidità;
	private int spazioLibero;
	
	//Singletone:
	private static Serra mySerra = null; 
	
	public Serra(){
				
		temperatura = 20;
		umidità = 60;
		spazioLibero = 36;
		giorno = 1;
	
	}
	
	public static Serra getInstance(){
		if(mySerra == null){
			mySerra = new Serra();
		}
		return mySerra;
	}

	//
	
	public double getTemperatura() { return temperatura;}
	public void setTemperatura(double newTemp) { temperatura = newTemp; } 
	
	public double getUmidità() { return umidità;}
	public void setUmidità(double newUm) { umidità = newUm; } 

	public int getSpazioLibero() { return spazioLibero;}
	public void setSpazioLibero(int newSpazio) { spazioLibero = newSpazio; } 

}
	