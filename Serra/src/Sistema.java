import java.util.InputMismatchException;
//import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Scanner;
import java.math.*;


public class Sistema {
	
	static Scanner userIn = new Scanner(System.in);  

	public static void comandi() {
		
		Serra serra = new Serra();
		GestisciSerra gestore = new GestisciSerra();
		PlantFactory plantFactory = new PlantFactory();
		

		
		gestore.visualizzaDati();
		
		boolean t = true;
		while( t == true){
		
		//Scanner userIn = new Scanner(System.in);
		
		// schermata comandi
		
		System.out.println("\n\n\n\n--------------- Comandi: ---------------\n"
				+ "\n • Inserisci 1 per modificare la temperatura\n"
				+ "\n • Inserisci 2 per modificare l'umidità\n"
				+ "\n • Inserisci 3 per visualizzare i dati attuali della serra\n"
				+ "\n • Inserisci 4 per aggiungere una pianta\n"
				+ "\n • Inserisci 5 per visualizzare i dati di una pianta\n"
				+ "\n • Inserisci 6 per rimuovere una pianta\n"
				+ "\n • Inserisci 7 per avanzare di un giorno\n"
				+ "\n • Inserisci 8 per uscire!\n\n");
		
		Scanner userInComm = new Scanner(System.in);
		
		if(userInComm.hasNextInt()){
		
			switch (userInComm.nextInt()) {
			
			case 1: // modifico temperatura
				
				System.out.println("\n--Hai scelto di modificare la temperatura--");
				System.out.println("\nInserisci 1 per aumetare la temperatura\n"
						+ "\nInserisci 0 per diminuire la temperatura\n"
						+ "(o un qualsiasi altro carattere)\n");
				Scanner userInTemp = new Scanner(System.in);
				
				if(userInTemp.hasNextInt()){
					gestore.modificaTemperatura(userInTemp.nextInt());
				}
				break;
			
			case 2: // modifico umidità
				
				Scanner userInUm = new Scanner(System.in);
				System.out.println("\n--Hai scelto di modifiare l'umidità--");
				System.out.println("\nInserisci 1 per aumetare l'umidità\n"
						+ "\nInserisci 0 per diminuirla\n"
						+ "(o un qualsiasi altro carattere)\n");
				
				if(userInUm.hasNextInt()){
					gestore.modificaUmidità(userInUm.nextInt());
				}
				break;
			
			case 3: //visualizzo dati serra
				
				System.out.println("\n--Hai scelto di visualizzare i dati attuali della serra--\n\n");
				gestore.visualizzaDati();
				break;
			
			case 4: // inserisco una pianta
				
				System.out.println("\n--Hai scelto di inserire una pianta--\n");
				System.out.println("\n - Quante celle occupa la pianta ?\n( valori ammessi : 1, 2, 4) ");
				
				Scanner userInCelle = new Scanner(System.in);
				if (userInCelle.hasNextInt()){
					
					int celle = userInCelle.nextInt();
					if(celle == 1 || celle == 2 || celle == 4 ){

						// controlla se c'è spazio
				   
						int [] pos = new int[2];
						int spOcc = celle;
						
						pos = gestore.checkSpazio( spOcc ); // <-------------------
						
						if(pos [0] == 0){
				
							System.out.println("\n ATTENZIONE : Non c'è spazio nella serra per iserire la pianta !\n");
				
						}else{
							
							System.out.println("\nLa pianta verrà inserita nella posizione:  "
									+ "riga: " + pos[0] + " colonna: " + (pos[1]-spOcc+1));
							System.out.println("\nSe vuoi inserire la pianta in questa posizione inserisci: ok" //aggiungere eccezione
									+ "\n altrimenti inserisci la posizione (riga,colonna) della prima cella occupata "
									+ "dalla pianta");
							
							//Posizione
							
							Scanner userInPos = new Scanner(System.in);
							
							if(userInPos.hasNext("ok")){  
								System.out.println("\nLa pianta verrà inserita nella posizione:  "
										+ "riga: " + pos[0] + " colonna: " + (pos[1]-spOcc+1));

							}else{
								
								pos[0] = userInPos.nextInt();
							
								int temp1 = userInPos.nextInt();
							
								pos[1]= temp1+spOcc-1;						
							}
							
							//Nome
							
							Scanner userInName = new Scanner(System.in);
							System.out.println("\n - Inserisci il nome della pianta:  ");			      						      				
							String nome = "";	
							if(userInName.hasNextLine()){ nome = userInName.nextLine(); }  
							
							//Altezza (con eccezione)
							
							System.out.println("\n-- Inserisci l'altezza della pianta in cm: -- ");
		      				int alt=0;
		      				boolean t2 = true;
		      				while(t2){
		      					int h = checkValidH();
		      					if(h != 0){
		      						alt = h;
		      						t2 = false;
		      					}
		      				}
			      				
							//Categoria
			      				
							System.out.println("\n - Che categoria di pianta vuoi inserire? \n (A: Temperata B: Tropicale C: Invernale) ");
							Scanner userInCat = new Scanner(System.in);
							String cat = "";	
			      			while (cat == "" ) {
			      				if(userInCat.hasNextLine()){
			      					String tipoDiPianta = userInCat.nextLine().toUpperCase();	
			      					if(tipoDiPianta.equals("B")) {			      							
			      						cat = "Tropicale";
						      			System.out.println("\n-- Hai scelto di inserire una pianta di tipo tropicale --\n ");	
			      					} else			      							
			      						if(tipoDiPianta.equals("A")) {			      								
			      							cat = "Temperata";
							      			System.out.println("\n-- Hai scelto di inserire una pianta di tipo temperata --\n ");		      						
			      						} else
			      							if(tipoDiPianta.equals("C")) {
			      								cat = "Invernale";
							      				System.out.println("\n-- Hai scelto di inserire una pianta di tipo invernale --\n ");
			      							} else {
			      								System.out.println("\n ATTENZIONE : Categoria  pianta non valido! \n Inserisci una categoria  valida: ");
			      							}
			      				}	
			      			}
			      			
						// Età (con eccezzione)
			      				
			      				System.out.println("\n-- Inserisci l'età della pianta: -- ");
			      				int eta=0;
			      				boolean t1 = true;
			      				while(t1){
			      					int age = checkValidAge();
			      					if(age != 0){
			      						eta = age;
			      						t1 = false;
			      					}
			      				}
			      				
			      				
			      				
			      				//PlantFactory plantFactory = new PlantFactory();
			    			    Pianta laPianta = null;
			    			    laPianta = plantFactory.makePianta(spOcc, nome, cat, eta, alt);
			      				gestore.inserisciPianta(laPianta, pos);	
			      				break;
						}
						}else{
							System.out.println("\n ATTENZIONE : grandezza non ammessa!!!");
							break;
						}
					}
				
			case 5: //visualizzazione dati pianta
				
				Scanner User1In = new Scanner(System.in);
				System.out.println("\nIn che posizione è la pianta che vuoi visualizzare?\n(inserisci le coordinate della pianta che vuoi visualizzare)");
				int x = User1In.nextInt();
				int y = User1In.nextInt();
				
				if(x > 3 || y > 13 ){
					while(x > 3 || y > 13){
						System.out.println("\nATTENZIONE: la posizione inserita non è valida perchè eccede i lmiti della serra"
								+ "\nINSERISCI UNA POSIZIONE VALIDA ALL'INTERNO DELLA SERRA");
						Scanner userInPos3 = new Scanner(System.in);
						x = userInPos3.nextInt();
						y = userInPos3.nextInt();
						
					}
				}
				
				gestore.visualizzaDatiPianta( x-1, y-1 );
				break;
				
			case 6: // rimuovi pianta
				
				Scanner User3In = new Scanner(System.in);
				System.out.println("\nIn che posizione è la pianta che vuoi rimuovere?\n(inserisci le coordinate della pianta che vuoi visualizzare)");
				int a = User3In.nextInt();
				int b = User3In.nextInt();
				
				if(a > 3 || b > 13 ){
					while(a > 3 || b > 13){
						System.out.println("\nATTENZIONE: la posizione inserita non è valida perchè eccede i lmiti della serra"
								+ "\nINSERISCI UNA POSIZIONE VALIDA ALL'INTERNO DELLA SERRA");
						Scanner userInPos3 = new Scanner(System.in);
						a = userInPos3.nextInt();
						b = userInPos3.nextInt();
						
					}
				}
				
				if(serra.getInstance().pianteSerra[a-1][b-1] != null){
					gestore.rimuoviPianta( a-1, b-1 );
					break;
				}
				else{
					System.out.println("\nATTENZIONE: la posizione inserita è vuota!");
					break;
				}
			
			case 7:  // avanzamento temporale
				
				gestore.avanzamentoTemporale();
				
				break;
			case 8: //exit
				System.out.println("\n**************** bye bye ****************");
				t = false;
				break;
    
			default: 
				System.out.println("\n**** COMANDO NON VALIDO ****");
            break;
				
			}
			}
		}	
	}

	
	public static int checkValidAge(){
		
		try{ 
			return userIn.nextInt();
			}
		catch (InputMismatchException e){
	
	userIn.next();
	System.out.println("ATTENZIONE : Età non valida!!!!\nInserisci un intero : ");
	
	return 0;
		}

	}
	
	public static int checkValidH(){
		
		try{ 
			return userIn.nextInt();
			}
		catch (InputMismatchException e){
	
	userIn.next();
	System.out.println("ATTENZIONE : Altezza non valida!!!!\nInserisci un intero : ");
	
	return 0;
		}

	}
	
	// 
	
	public static void main(String args[]){
		
		Sistema newSistema = new Sistema();
		newSistema.comandi();
				
		
	}


}	
