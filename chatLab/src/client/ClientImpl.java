package client;

import common.*;

import javax.swing.undo.CannotRedoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by giuseppetesta on 01/06/16.
 */
public class ClientImpl extends UnicastRemoteObject implements Client {

    private static final long serialVersionUID = -7192644237492872987L;

    protected final User user;
    protected final Server server;
    protected final Set<MessageObserver> observers = new HashSet<MessageObserver>();


    public ClientImpl(String name, Server server) throws CannotRegisterException, RemoteException {
        super();
        //nome e server col quale deve comunicare
        this.user = new User(name);
        this.server = server;

        try {
            server.registerClient(this);
            System.out.println(this.getUser().getName()+ " logged");
        } catch (RemoteException e) {
            System.out.println("Impossibile connettersi al server");
            throw new CannotRedoException();
        }
    }

    @Override
    public String toString() {
        return user.getName();
    }

    @Override
    public User getUser() throws RemoteException {
        return user;
    }

    public void addObserver( MessageObserver mo ) {
        observers.add(mo);
    }

    @Override
    public void notify(Message message) throws RemoteException {
        for (MessageObserver observer : observers) {
            observer.notify(message);
        }
    }

    protected Message createMessage(String message) throws RemoteException {
        return new Message(message, this.getUser());
    }

    public void broadcastMessage(String message) throws RemoteException {
        Message m = createMessage(message);
        this.server.broadcastMessage(m);
    }


    public void sendPrivateMessage(String name, String message) throws RemoteException {
        Message m = createMessage(message);
        server.sendPrivateMessage(name, m);
    }

    /*
    public void joinGroup(String name) throws RemoteException {
        server.joinGroup(name, this);
    }

    public void leaveGroup(String name) throws RemoteException {
        server.leaveGroup(name, this);
    }

    public void groupMessage(String name, String message) throws RemoteException {
        Message m = createMessage(message);
        server.groupMessage(name, m);
        */



}
