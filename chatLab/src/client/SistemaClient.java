package client;

import common.Client;
import common.Server;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

/**
 * @author Giuseppe Testa
 * 01/06/16
 */
public class SistemaClient {

    public static void printMenu() {
        System.out.println();
        System.out.println("1 - Broadcast message");
        //System.out.println("2 - Join goup");
        //System.out.println("3 - Leave goup");
        //System.out.println("4 - Group message");
        System.out.println("5 - Private message");
        System.out.println("0 - Exit");
        System.out.println();
    }

    public static void broadcastMessage(Scanner s, ClientImpl client) {
        System.out.println("Enter message to broadcast:");
        String message = s.nextLine();
        try {
            client.broadcastMessage(message);
        } catch (RemoteException e) {
            System.out.println("Cannot broadcast message");
        }
    }

    /*
    public static void joinGroup(Scanner s, Client client) {
        System.out.println("Enter group name to join:");
        String name = s.nextLine();
        try {
            client.joinGroup(name);
        } catch (RemoteException e) {
            System.out.println("Cannot join group");
        }
    }

    public static void leaveGroup(Scanner s, Client client) {
        System.out.println("Enter group name to leave:");
        String name = s.nextLine();
        try {
            client.leaveGroup(name);
        } catch (RemoteException e) {
            System.out.println("Cannot leave group");
        }
    }

    public static void groupMessage(Scanner s, ChatClient client) {
        System.out.println("Enter group name to send the message to:");
        String group = s.nextLine();
        System.out.println("Enter the message:");
        String message = s.nextLine();
        try {
            client.groupMessage(group, message);
        } catch (RemoteException e) {
            System.out.println("Cannot send group messages");
        }
    }*/

    public static void privateMessage(Scanner s, ClientImpl client) {
        System.out.println("Enter user name to send the message to:");
        String user = s.nextLine();
        System.out.println("Enter the message:");
        String message = s.nextLine();
        try {
            client.sendPrivateMessage(user, message);
        } catch (RemoteException e) {
            System.out.println("Cannot send private messages");
        }
    }

    /** Run with - Djava.security.policy=client.policy */

    public static void main(String[] args) throws NotBoundException, IOException, CannotRegisterException {
        /*if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }*/ // ???
        try {
            Registry registry = LocateRegistry.getRegistry( 12345 );
            Server server = (Server) registry.lookup("chat");
            String nomeClient = "Luca" + (int)(Math.random() * 100);
            ClientImpl client = new ClientImpl( nomeClient, server);

            Scanner s = new Scanner(System.in);

            while (true) {
                printMenu();
                int choice = s.nextInt();
                if (choice == 1) {
                    broadcastMessage(s, client);
                } /*else if (choice == 2) {
                    joinGroup(s, client);
                } else if (choice == 3) {
                    leaveGroup(s, client);
                } else if (choice == 4) {
                    groupMessage(s, client);
                }*/ else if (choice == 5) {
                    privateMessage(s, client);
                } else if (choice == 0) {
                    break; // Exit from loop
                }
            }
            s.close();
            server.unregisterClient(client);
            System.exit(0); // bye

            server.unregisterClient(client);
            UnicastRemoteObject.unexportObject(client, false);
            System.out.println("Session ended");

        } catch (RemoteException e){
            System.err.println("SistemaClient exception: ");
            e.printStackTrace();
        } catch (NotBoundException e) {
        e.printStackTrace();
        }
    }
}
