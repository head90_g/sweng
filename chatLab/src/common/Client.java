package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by giuseppetesta on 01/06/16.
 */

public interface Client extends Remote {

    //metodi che il server puo invocare sul client

    void notify(Message message) throws RemoteException;

    User getUser() throws RemoteException;
}
