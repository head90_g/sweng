package common;

import java.io.Serializable;

/**
 * Created by giuseppetesta on 04/06/16.
 */

public class Message implements Serializable {
    private static final long serialVersionUID = 8400214413734171621L;

    private final String message;
    private final User author;

    public Message( String message, User author ) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }
    public User getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return String.format( "[%s] - %s", author, message);
    }
}
