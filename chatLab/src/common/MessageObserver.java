package common;

/**
 * Created by giuseppetesta on 19/06/16.
 */

public interface MessageObserver {
    void notify( Message message );
}
