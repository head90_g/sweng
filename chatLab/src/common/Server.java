package common;


import client.ClientImpl;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by giuseppetesta on 01/06/16.
 */
public interface Server extends Remote {

    //metodi che il client puo invocare sul server

    void registerClient(Client client) throws RemoteException;

    void unregisterClient(ClientImpl client) throws RemoteException;

    void greet(String name) throws RemoteException;

    String returnGreet(String name) throws RemoteException;

    //void sendMessage(Client writer, String message) throws RemoteException;

    void broadcastMessage(Message m) throws RemoteException;

    void sendPrivateMessage(String name, Message m) throws RemoteException;




    /*
	public void joinGroup(String name, ChatClientInterface user) throws RemoteException;
	public void leaveGroup(String name, ChatClientInterface user) throws RemoteException;
    void groupMessage(String name, Message m) throws RemoteException;
*/
}
