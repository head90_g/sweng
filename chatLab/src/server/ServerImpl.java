package server;

import client.ClientImpl;
import common.Client;
import common.Message;
import common.Server;
import common.User;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by giuseppetesta on 01/06/16.
 */

public class ServerImpl extends UnicastRemoteObject implements Server {

    private static final long serialVersionUID = 8573201216151204858L;

    private ArrayList<Client> clients;
    //protected final Map<String, List<ChatClientInterface>> groups = new HashMap<String, List<ChatClientInterface>>();

    public ServerImpl() throws RemoteException{
        super();
        this.clients = new ArrayList<Client>();
    }

    @Override
    public void registerClient(Client client) throws RemoteException {
        if( !clients.contains(client) ) {
            clients.add(client);
            System.out.format("Removing client: %s\n", client.getUser());
        }

    }

    @Override
    public void unregisterClient(ClientImpl client) throws RemoteException {
        clients.remove(client);
        System.out.println("SistemaClient: " + client.getUser().getName() + " logout");
    }

    /*@Override
    public void sendMessage(Client writer, String message) throws RemoteException{
        ArrayList<Client> dead = new ArrayList<Client>();

        for(Client client:clients){

            int i= clients.indexOf(writer);
            try {
                String msg = writer.getName()+": "+ message;
                //System.out.println(msg);
                if(clients.indexOf(client) != i) { clients.get(clients.indexOf(client)).notify(msg); };

                //client.notify(msg);
            } catch (RemoteException e){
                e.printStackTrace();
                dead.add(client);
            }
        }
        for( Client c:dead){
            this.clients.remove(c);
        }

    } */

    @Override
    public void greet(String name) throws RemoteException{
        System.out.println(name + " connesso\n");
    }

    @Override
    public String returnGreet(String name) throws RemoteException{
        return "ciao "+ name;
    }

    public void sendMessage(List<Client> clients, Message m) throws RemoteException {
        if (clients != null) {
            for (Client client : clients) {
                sendMessage(client, m);
            }
        }
    }

    public void sendMessage(String user, Message m) throws RemoteException {
        for (Client client : clients) {
            if (client.getUser().getName().equals(user)) {
                sendMessage(client, m);
                break; // Exit
            }
        }
    }

    public void sendMessage(Client client, Message m) throws RemoteException {
        User author = m.getAuthor();
        User destAuthor = client.getUser();
        // Do not send to self
        if( !author.getName().equals(destAuthor.getName())) {
            client.notify(m);
        }
    }

    @Override
    public void broadcastMessage(Message m) throws RemoteException {
        System.out.format("Broadcasting message for %s\n", m.getAuthor());
        sendMessage(clients, m);
    }

    @Override
    public void sendPrivateMessage(String name, Message m) throws RemoteException {
        sendMessage(name, m);
    }

    /*
        @Override
    public void groupMessage(String name, Message m) throws RemoteException {
        List<Client> members = groups.get(name);
        sendMessage(members, m);
    }

    @Override
    public void joinGroup(String name, Client user) throws RemoteException {
        // Check if the user is already in the group
        List<Client> members = groups.get(name);

        if (members != null) {
            // No group create
            members = new ArrayList<Client>();
            groups.put(name, members);
        }

        if (!members.contains(user)) {
            // Member not present, add
            members.add(user);
        }
    }

    @Override
    public void leaveGroup(String name, ClientInterface user) throws RemoteException {
        // Check if the user is already in the group
        List<Client> members = groups.get(name);

        if (members != null) {
            members.remove(user);
        }

    }*/

}