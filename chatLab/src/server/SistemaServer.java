package server;

import common.Server;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class SistemaServer {

    private static final long serialVersionUID = 8553201216151204858L;

    /** Run with:
     -Djava.security.policy=server.policy
     -Djava.rmi.server.codebase="file://${workspace_loc:/SistemaServer}/bin/ file://${workspace_loc:/Common}/bin/"
     */

    public static void main(String[] args) throws RemoteException {
        /*
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }*/ //???

        try {
            /* create and start registry */
            Registry registry = LocateRegistry.createRegistry( 12345 );
            Server chat = new ServerImpl();
            registry.rebind( "chat",  chat);

            System.out.println("SistemaServer online...\n");


        } catch (RemoteException e) {
            System.err.println("SistemaServer exception. ");
            e.printStackTrace();
        }
    }
}


