package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by giuseppetesta on 31/05/16.
 */
public interface ServerInterface extends Remote {

    public ArrayList<String> getFiles() throws RemoteException;

    public String readFiles(String path) throws RemoteException;

    //public boolean ...

}
