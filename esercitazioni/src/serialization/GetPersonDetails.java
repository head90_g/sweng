package serialization;

/**
 *
 * @author Luca Galli <lgalli@elet.polimi.it>
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
public class GetPersonDetails {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        String filename = "person.txt";
        ArrayList<PersonDetails> pDetails = null;
        FileInputStream fis = null;
        ObjectInputStream in = null;
        try {
            fis = new FileInputStream(filename);
            in = new ObjectInputStream(fis);
            pDetails = (ArrayList<PersonDetails>) in.readObject();
            in.close();
            fis.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        // print out the size
        System.out.println("Person Details Size: " + pDetails.size());
        for(PersonDetails p : pDetails)
        {
            System.out.println(p.getName()+" is a "+p.getSex()+" and is "+p.getAge()+" years old.");
        }
        System.out.println();
    }
}