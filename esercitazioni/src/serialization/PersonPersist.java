package serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Luca Galli <lgalli@elet.polimi.it>
 */
public class PersonPersist {

    public static void main(String[] args) {
        String filename = "person.txt";
        PersonDetails person1 = new PersonDetails("Luca", 27, "Male");
        PersonDetails person2 = new PersonDetails("Sofia", 14, "Male");
        PersonDetails person3 = new PersonDetails("Antonio", 95, "Male");
        ArrayList<PersonDetails> list = new ArrayList<>();
        list.add(person1);
        list.add(person2);
        list.add(person3);
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = new FileOutputStream(filename);
            out = new ObjectOutputStream(fos);
            out.writeObject(list);
            out.close();
            fos.close();
            System.out.println("Object Persisted");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
