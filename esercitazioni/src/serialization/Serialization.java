package serialization;

/**
 * Created by giuseppetesta on 31/05/16.
 */
public class Serialization {

    /* basta soltanto che la classe che vogliamo sia serializzabile/deser implementi Serializable
     *
     * private static final long serialVersionUID = 1L;
     * quando poi vado a cmabiare la classe, e.g. aggiungo un campo, vado a modificare questo campo
     * 1l -> 2L
     *
     */
}
